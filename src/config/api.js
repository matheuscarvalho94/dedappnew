// import axios from "axios";

// export default axios.create({
//   // baseURL: "192.168.15.19:80/api",
// });

import axios from 'axios';
import {AsyncStorage} from 'react-native';
import Store from '../store';

const api = axios.create({
  baseURL: "http://dedapp.com.200-170-82-248.wplsk10.com.br/api",
});

api.interceptors.request.use(async config => {
  const token = await AsyncStorage.getItem('@token');
  // const token = undefined;
  if (token) {
    let tokenformatado = token.replace('"', '').replace('"', '')
    config.headers.Authorization = `Bearer ${tokenformatado}`;
  } else if(Store.getState().token.data.token){
    config.headers.Authorization = `Bearer ${Store.getState().token.data.token}`;
  }
  return config;
});

export default api;
