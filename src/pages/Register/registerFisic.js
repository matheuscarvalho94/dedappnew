import React, {useState} from 'react';
import {SafeAreaView, ScrollView, View, Alert, StyleSheet, Platform} from 'react-native';
import {Input} from '../../component/Input';
import colors from '../../styles/colors';
import {ContentBg} from '../../styles/general';
import {
  H1,
  Span,
  Button,
  TextButton,
  Container,
  Label,
  styles,
} from './styles';
import Header from '../../component/Header';
import Radio from '../../component/Radio';
import PickerSelect from '../../component/Picker';
import RNPicker from '../../component/RNPicker';
import {OpacityBar} from '../../component/OpacityBar';
import RNPickerSelect from 'react-native-picker-select';

import {TextInputMask} from 'react-native-masked-text';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import RegisterActions from '../../store/ducks/registerFisic';
import {IndicatorLoading} from '../Login/styles';
import { fonts } from '../../styles';

const stateCivil = [
  {
    value: 'Solteiro(a)',
    label: 'SOLTEIRO(A)',
  },
  {
    value: 'Casado(a)',
    label: 'CASADO(A)',
  },
  {
    value: 'Divorciado(a)',
    label: 'DIVORCIADO(A)',
  },
  {
    value: 'Separado(a)',
    label: 'SEPARADO(A)',
  },
  {
    value: 'Viúvo(a)',
    label: 'VIÚVO(A)',
  },
];

const stateFormacao = [
  {
    value: 'Arquitetura',
    label: 'ARQUITETURA',
  },
  {
    value: 'Designer de Interiores',
    label: 'DESIGNER DE INTERIORES',
  },
  {
    value: 'Paisagista',
    label: 'PAISAGISTA',
  },
  {
    value: 'Decorador',
    label: 'DECORADOR',
  },
  {
    value: 'Outros',
    label: 'OUTROS',
  },
];

const stateAssociacao = [
  {
    value: 'CREA',
    label: 'CREA',
  },
  {
    value: 'ABD',
    label: 'ABD',
  },
  {
    value: 'ASBEA',
    label: 'ASBEA',
  },
  {
    value: 'CAU',
    label: 'CAU',
  },
  {
    value: 'Outros',
    label: 'Outros',
  },
];

const RegisterFisicScene = ({navigation, addRegisterRequest, register}) => {
  const _register = () => {
    const body = {
      nome,
      sobrenome,
      email,
      sexo1,
      sexo2,
      estadoCivil,
      dataNascimento,
      cpf,
      password,
      tipo,
      dataCadastro,
      codigo,
      aprovado,
      formacao,
      associacao,
      cep,
      cidade,
      uf: estado,
      numero,
      complemento,
      endereco,
      bairro,
      celular,
    };

    //console.log(body)

    if (nome === '') {
      Alert.alert('D&D', 'Digite o seu nome.');
    } else if (email === '') {
      Alert.alert('D&D', 'Digite seu email.');
    } else if (password === '') {
      Alert.alert('D&D', 'Digite sua senha.');
    } else {
      addRegisterRequest(body);
    }
  };

  const [nome, setNome] = useState('');
  const [sobrenome, setSobrenome] = useState('');
  const [email, setEmail] = useState('');
  const [sexo1, setSexo1] = useState(false);
  const [sexo2, setSexo2] = useState(true);
  const [dataNascimento, setDataNascimento] = useState('');
  const [estadoCivil, setEstadoCivil] = useState('');
  const [cpf, setCpf] = useState('');

  const [password, setPassword] = useState('');
  const [tipo, setTipo] = useState('PF');
  const [dataCadastro, setDataCadastro] = useState('2019-01-09');
  const [codigo, setCodigo] = useState('123');
  const [aprovado, setAprovado] = useState('1');

  const [celular, setCelular] = useState('');
  const [formacao, setFormacao] = useState('');
  const [associacao, setAssociacao] = useState('');
  const [cep, setCep] = useState('');
  const [estado, setEstado] = useState('');
  const [cidade, setCidade] = useState('');
  const [bairro, setBairro] = useState('');
  const [endereco, setEndereco] = useState('');
  const [numero, setNumero] = useState('');
  const [complemento, setComplemento] = useState('');

  function onSetSexo1() {
    setSexo1(true);
    setSexo2(false);
  }

  function onSetSexo2() {
    setSexo1(false);
    setSexo2(true);
  }

  function valueChange(value) {
    setEstadoCivil(value);
  }

  function valueChangeForm(value) {
    setFormacao(value);
  }

  function valueChangeAsso(value) {
    setAssociacao(value);
  }

  // function getOffset() {
  //   console.warn(this.refs.myScrollView.scrollProperties.offset);
  // }

  function onScrollEndSnapToEdge(event) {
    const y = event.nativeEvent.contentOffset.y;
    console.warn(y);
  }

  return (
    <ContentBg>
      <Header navigation={navigation} nav={false} />
      <ScrollView>
        <Container>
          <H1>SOBRE VOCÊ</H1>
          <Span style={{marginBottom: 20}}>PRRENCHA OS CAMPOS ABAIXO</Span>
          <Input
            placeholder="NOME"
            value={nome}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setNome(txt)}
          />
          <Input
            placeholder="SOBRENOME"
            value={sobrenome}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setSobrenome(txt)}
          />

          <View style={{marginTop: 15, marginBottom: 30}}>
            <Label>Sexo</Label>
            <View style={{flexDirection: 'row'}}>
              <Radio
                label="FEMININO"
                value={sexo1}
                onPress={() => onSetSexo1()}
              />
              <Radio
                label="MASCULINO"
                value={sexo2}
                onPress={() => onSetSexo2()}
              />
            </View>
          </View>

          <View style={[stylesRNP.containerSelect]}>
            <RNPickerSelect
              placeholder={{
                label: 'ESTADO CIVIL',
                color: colors.white50,
                value: '',
              }}
              style={pickerStyle}
              onValueChange={value => valueChange(value)}
              items={stateCivil}
              value={estadoCivil}
            />
          </View>

          {/* <RNPicker
            label="ESTADO CIVIL"
            value={estadoCivil}
            list={stateCivil}
            valueChange={valueChange}
            placeholderTextColor={colors.white50}
            items={stateCivil}></RNPicker> */}

          {/* <PickerSelect
            label="ESTADO CIVIL"
            value={estadoCivil}
            list={stateCivil}
            valueChange={valueChange}
            placeholderTextColor={colors.white50}
          /> */}

          <TextInputMask
            type={'datetime'}
            options={{
              format: 'DD/MM/YYYY',
            }}
            value={dataNascimento}
            style={styles.input}
            onChangeText={text => setDataNascimento(text)}
            placeholderTextColor={colors.white50}
            placeholder="DATA DE NASCIMENTO"
          />

          <Input
            placeholder="EMAIL"
            value={email}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setEmail(txt)}
          />

          <TextInputMask
            type={'cpf'}
            value={cpf}
            style={styles.input}
            onChangeText={text => setCpf(text)}
            placeholderTextColor={colors.white50}
            placeholder="CPF"
          />

          <TextInputMask
            type={'cel-phone'}
            options={{
              maskType: 'BRL',
              withDDD: true,
              dddMask: '(99) ',
            }}
            value={celular}
            style={styles.input}
            onChangeText={text => setCelular(text)}
            placeholderTextColor={colors.white50}
            placeholder="CELULAR"
          />

          <View style={[stylesRNP.containerSelect]}>
            <RNPickerSelect
              placeholder={{
                label: 'FORMAÇÃO',
                value: '',
                color: colors.white50,
              }}
              style={pickerStyle}
              onValueChange={value => valueChangeForm(value)}
              items={stateFormacao}
              value={formacao}
            />
          </View>

          {/* <RNPicker
            label="FORMAÇÃO"
            value={formacao}
            list={stateFormacao}
            valueChange={valueChangeForm}
            placeholderTextColor={colors.white50}
          />

          <RNPicker
            label="ASSOCIAÇÃO"
            value={associacao}
            list={stateAssociacao}
            valueChange={valueChangeAsso}
            placeholderTextColor={colors.white50}
          /> */}

          <View style={[stylesRNP.containerSelect]}>
            <RNPickerSelect
              placeholder={{
                label: 'ASSOCIAÇÃO',
                color: colors.white50,
                value: '',
              }}
              style={pickerStyle}
              onValueChange={value => valueChangeAsso(value)}
              items={stateAssociacao}
              value={associacao}
            />
          </View>

          <TextInputMask
            type={'zip-code'}
            value={cep}
            style={styles.input}
            onChangeText={text => setCep(text)}
            placeholderTextColor={colors.white50}
            placeholder="CEP"
          />

          <Input
            value={endereco}
            style={styles.input}
            onChangeText={text => setEndereco(text)}
            placeholderTextColor={colors.white50}
            placeholder="ENDEREÇO"
          />

          <Input
            placeholder="ESTADO"
            value={estado}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setEstado(txt)}
          />

          <Input
            placeholder="CIDADE"
            value={cidade}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setCidade(txt)}
          />

          <Input
            placeholder="BAIRRO"
            value={bairro}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setBairro(txt)}
          />

          <Input
            placeholder="NÚMERO"
            value={numero}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setNumero(txt)}
          />

          <Input
            placeholder="COMPLEMENTO"
            value={complemento}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setComplemento(txt)}
          />

          <Input
            placeholder="SENHA"
            value={password}
            secureTextEntry={true}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setPassword(txt)}
          />

          <Button onPress={() => _register()}>
            <TextButton> ENVIAR </TextButton>
            {register.loading && (
              <IndicatorLoading size="small" color={colors.white} />
            )}
          </Button>
        </Container>
      </ScrollView>
      <OpacityBar />
    </ContentBg>
  );
};


const pickerStyle = {
  font: fonts.tradeconde18,
	placeholderColor: 'white',
  placeholder: {
    fontSize: 16,
    fontFamily: fonts.tradeconde18,
  },
  inputIOS: {
    color: colors.white50,
    fontFamily: fonts.tradeconde18,
    fontSize: 14,
    padding: 0,
	},
	inputAndroid: {
    color: colors.white50,
    fontFamily: fonts.tradeconde18,
    fontSize: 14,
    padding: 0,
	},
};


const mapStateToProps = state => ({
  register: state.register,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(RegisterActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterFisicScene);

const stylesRNP = StyleSheet.create({
  containerSelect: {
    backgroundColor: 'transparent',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.white,
    color: colors.white,
    height: 60,
    marginBottom: 15,
    paddingHorizontal: 20,
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  select: {
    height: 35,
    //fontFamily: fonts.tradeconde18,
    fontSize: 12,
    color: colors.white50,
    width: '100%',
  },
});
