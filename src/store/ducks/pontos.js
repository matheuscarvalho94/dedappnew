import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* Types & Action Creators */
const { Types, Creators } = createActions({
  addPontosRequest: ["action"],
  addPontosSuccess: ["pontos"],
  addPontosFailure: ["pontos"]
});


export const PontosTypes = Types;
export default Creators;

/* Initial State */

export const INITIAL_STATE = Immutable({
  data: [],
  loading: false
});

/* Reducers */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_PONTOS_REQUEST]: state => state.merge({ loading: true }),
  [Types.ADD_PONTOS_SUCCESS]: (state: Object, { pontos }: any) => state.merge({ loading: false,  data: pontos }),
  [Types.ADD_PONTOS_FAILURE]: (state: Object, { pontos }: any) => state.merge({ loading: false,  data: pontos }),

});