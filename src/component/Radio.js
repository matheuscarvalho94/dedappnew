import React from 'react';
import { View } from 'react-native';

import styled from 'styled-components';
import colors from '../styles/colors';
import fonts from '../styles/fonts';
import { Platform } from 'react-native';

const Radio = ({
  label,
  value,
  onPress,
}) => {
  return (
    <Row onPress={onPress}>
      <RadioButton>
        {value && (<Container />)}
      </RadioButton>
      <Label>{label}</Label>
    </Row>
  );
};

export const Row = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
`;

export const RadioButton = styled.View`
  border: 1px solid #fff;
  width: 30px;
  height: 30px;
  background: transparent;
  color: ${colors.white50};
  border-radius: 100px;
`;

export const Container = styled.View`
  background: #fff;
  width: 30px;
  height: 30px;
  border-radius: 100px;
`;

export const Label = styled.Text`
  font-size: 16px;
  font-family: ${fonts.tradebold};
  color: ${colors.white};
  margin-left: 10px;
  margin-right: 35px;
`;

export default Radio;
