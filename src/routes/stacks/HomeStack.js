import { createStackNavigator } from 'react-navigation';
import HomeScene from '../../pages/Home/index';

export const HomeStackNavigator = createStackNavigator({
  HomeScene: {
    screen: HomeScene,
    navigationOptions: {
      header: null
    }
  }
})