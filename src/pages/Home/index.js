import React, {useState, useEffect} from 'react';
import {H1, H2, H3, Span, Span2} from './styles';
import {Content, Container, ListView, WrapView, ContentList, Title, Value, Border, Scroll} from '../../styles/general';
import {colors}  from '../../styles';
import Header from '../../component/Header';

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PontosActions from "../../store/ducks/pontos";
import MyDataActions from '../../store/ducks/mydata';
import moment from "moment";
import Store from '../../store';
import {AsyncStorage} from 'react-native';

const HomeScene = ({navigation, pontos, addPontosRequest, addGetdataRequest, mydata}) => {
  const [nome, setNome] = useState('')

  async function loadCodigo() {
    const codigo = await AsyncStorage.getItem('@codigo');
    console.log(codigo, 'code');
    addPontosRequest(codigo);
    addGetdataRequest(codigo);
  }

  useEffect(() => {
    loadCodigo();
  }, []);

  useEffect(() => {
    // console.log(Store.getState(), 'pontos');
    if (mydata.dataadapter.userViewModel) {
      setNome(mydata.dataadapter.userViewModel.Nome);
    }
  }, [mydata])
  return (
    <Content>
      <Header navigation={navigation} />
      <Scroll>
        <Container style={{paddingBottom: 0}}>
          <H1>OLÁ</H1>
          <H2>{nome}</H2>
          <Span>LOREM IMPSUN DOLOR SIT AMET, CONSECTEUR ADISCIPING ELIT, SED DO EIU LOREM IMPSUN DOLOT IT SIT ATMEET, CONSECTEUR ADISCIPING ELIT, SED DO EIU</Span>
          <H3>ÚLTIMOS PONTOS</H3>
          {
            pontos.data.length > 0
            ? <ListView
            data={pontos.data}
            renderItem={
              ({item, index}) => 
                ListRender(item, index)
              }
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{
              borderLeft: 1,
              marginLeft: -5,
              borderColor: colors.secondary,
              paddingLeft: 10,
              flex: 1,
            }}
          />
          :<Span2>Você não possui histórico de pontos.</Span2>
          }
          
        </Container>
      </Scroll>
    </Content>
  );
};

const ListRender = (item, index) => {
  let dateformat = moment(item.Date).format('ll');
  let dateSplit = dateformat.split(",")
  return (
    <WrapView key={index}>
      <Border></Border>
      <ContentList>
        <Title>{item.Descricao}</Title>
        <Value>{item.Valor}</Value>
        <Title>{item.Pontuacao} PONTOS</Title>
      </ContentList>
      <Title>{dateSplit[0]}</Title>
    </WrapView>
  );
}

const mapStateToProps = state => ({
  pontos: state.pontos,
  mydata: state.mydata,
});

const mapDispatchToProps = dispatch =>
bindActionCreators({
    ...PontosActions,
    ...MyDataActions,
}, dispatch);


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScene);