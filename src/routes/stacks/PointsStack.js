import { createStackNavigator } from 'react-navigation';
import PointsScene from '../../pages/Points';

export const PointsStackNavigator = createStackNavigator({
  PointsScene: {
    screen: PointsScene,
    navigationOptions: {
      header: null
    }
  }
})