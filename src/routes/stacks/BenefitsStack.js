import { createStackNavigator } from 'react-navigation';
import BenefitsScene from '../../pages/Benefits';

export const BenefitsStackNavigator = createStackNavigator({
  BenefitsScene: {
    screen: BenefitsScene,
    navigationOptions: {
      header: null
    }
  }
})