import React from 'react';
import { View } from 'react-native';
import styled from 'styled-components';
import { colors } from '../styles';

const Header = ({navigation, nav}) => (
  <ViewHeader>
    {/* <Button onPress={() => navigation.goBack()}>
      <BackButton source={require('../assets/backButton.png')} resizeMode='contain' />
    </Button> */}
    <Logo source={require('../assets/logo.png')} resizeMode="contain" />
    {/* {nav ? (
      <Button onPress={() => navigation.toggleDrawer()}>
        <Nav source={require('../assets/navbar.png')} resizeMode="contain" />
      </Button>
    ) : (
      <View style={{ width: 100 }} />
    )} */}
  </ViewHeader>
);

export const ViewHeader = styled.View`
  height: 85px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: ${colors.black};
  width: 100%;
  padding: 0 15px;
`;

export const Button = styled.TouchableOpacity`
  flex-grow: 1;
`;

export const BackButton = styled.Image`
  width: 25px;
  height: 25px;
`;

export const Nav = styled.Image`
  width: 100%;
  height: 25px;
`;

export const Logo = styled.Image`
  flex-grow: 1;
  text-align: center;
  width: 80px;
  height: 40px;
`;

export default Header;
