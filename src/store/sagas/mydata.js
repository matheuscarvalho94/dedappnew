import {call, put} from 'redux-saga/effects';
import api from '../../config/api';
import api2 from '../../config/api2';
import queryString from 'query-string';
import {Alert} from 'react-native';
import MyDataActions from '../ducks/mydata';

export function* getData(data) {
  //console.log("veio akiii", data.mydata)

  let body = queryString.stringify({cod: data.mydata});

  try {

    const response = yield call(api.post, `/meusdados/buscardados`, body);
    yield put(MyDataActions.addGetdataSuccess(response.data));

  } catch (err) {

    yield put(MyDataActions.addMydataFailure(err));

  }
}

export function* postMyData(props) {
  const body = queryString.stringify({
    nome: props.action.nome,
    sobrenome: props.action.sobrenome,
    sexo: props.action.sexo1 === true ? 'Masculino' : 'Feminino',
    estadoCivil: props.action.estadoCivil,
    nascimento: props.action.dataNascimento,
    email: props.action.email,
    login: props.action.email,
    cpfcnpj: props.action.cpf,
    senha: props.action.password,
    tipo: props.action.tipo,
    dataCadastro: props.action.dataCadastro,
    codigo: props.action.codigo,
    cod: props.action.cod,
    aprovado: props.action.aprovado,
    formacao: props.action.formacao,
    celular: props.action.celular,
    endereco: props.action.endereco,
    cep: props.action.cep,
    bairro: props.action.bairro,
    numero: props.action.numero,
    cidade: props.action.cidade,
    uf: props.action.uf,
  });

  try {
    const response = yield call(api.post, `/MeusDados/UpdateDados`, body);
    yield put(MyDataActions.addMydataSuccess(response.data));
    Alert.alert('D&D', 'Dados atualizados com sucesso!');
  } catch (err) {
    console.log(err, `errorrr`);
    yield put(MyDataActions.addMydataFailure(err));
    Alert.alert('D&D', 'Ocorreu algum problema. Tente novamente');
  }
}
