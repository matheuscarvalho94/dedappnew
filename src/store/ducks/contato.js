import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* Types & Action Creators */
const { Types, Creators } = createActions({
  addContatoRequest: ["action"],
  addContatoSuccess: ["contato"],
  addContatoFailure: ["contato"]
});

export const ContatoTypes = Types;
export default Creators;

/* Initial State */

export const INITIAL_STATE = Immutable({
  data: [],
  loading: false
});

/* Reducers */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_CONTATO_REQUEST]: state => state.merge({ loading: true }),
  [Types.ADD_CONTATO_SUCCESS]: (state: Object, { contato }: any) => state.merge({ loading: false,  data: contato }),
  [Types.ADD_CONTATO_FAILURE]: (state: Object, { contato }: any) => state.merge({ loading: false,  data: contato }),

});