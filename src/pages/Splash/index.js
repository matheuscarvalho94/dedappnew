import React, {useState, useEffect} from 'react';
import {
  StatusBar, View, ActivityIndicator
} from 'react-native';
import {AsyncStorage} from 'react-native';
import {styles, Logo} from './styles';
import {colors} from '../../styles';
import { FraseRodape, ViewRodape } from '../../styles/general';
import { NavigationActions, StackActions } from 'react-navigation'; 
import { navigatorRef } from '../../index';
import SplashScreen from 'react-native-splash-screen';

const Splash = ({navigation}) => {
  const [progress, setprogress] = useState(0);
  useEffect(() => {
    async function getToken() {
      const value = await AsyncStorage.getItem('@token');
      if (!value) {
        let routePage = 'LoginScene';
        const nav = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: routePage
            })
          ],
          key: null
        });
        navigatorRef.dispatch(nav);
        SplashScreen.hide();
      } else {
        let routePage = 'MainDrawer';
        const nav = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: routePage
            })
          ],
          key: null
        });
        navigatorRef.dispatch(nav);
        SplashScreen.hide();
      }
    }
    setTimeout(() => {
      getToken()
    }, 1000);
  }, [])

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor={'#000'} />
      <View style={styles.progressContainer}>
        <Logo
          source={require('../../assets/logo.png')}
          resizeMode="contain"
        />
        <ActivityIndicator size="large" color="#fff" />
      </View>
      <ViewRodape>
        <FraseRodape>RELACIONAMENTO D&D 2019. TODOS OS DIREITOS RESERVADOS</FraseRodape>
      </ViewRodape>
    </View>
  );
};

export default Splash;
