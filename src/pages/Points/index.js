import React, {useEffect} from 'react';
import {StatusBar} from 'react-native';
import {H1, H2, H3, Span, Span2} from './styles';
import {Content, Container, ListView, WrapView, ContentList, Title, Value, Border, Scroll} from '../../styles/general';
import {colors}  from '../../styles';
import Header from '../../component/Header';

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PontosActions from "../../store/ducks/pontos";
import moment from "moment";
import Store from '../../store';

const PointsScene = ({ navigation, pontos, addPontosRequest }) => {
  useEffect(() => {
    addPontosRequest(Store.getState().mydata.dataadapter.userViewModel.Cod);
  }, []);
  useEffect(() => {
    console.log(pontos, 'pontos')
  }, [pontos])
  return (
    <Content>
      <Header navigation={navigation} />
      <Scroll>
        <Container style={{paddingBottom: 0}}>
          <H2>ÚLTIMOS{`\n`}PONTOS</H2>
          <Span>LOREM IMPSUN DOLOR SIT AMET, CONSECTEUR ADISCIPING ELIT, SED DO EIU LOREM IMPSUN DOLOT IT SIT ATMEET, CONSECTEUR ADISCIPING ELIT, SED DO EIU</Span>

          {
            pontos.data.length > 0
            ? <ListView
            data={pontos.data}
            renderItem={
              ({item, index}) => 
                ListRender(item, index)
              }
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{
              borderLeft: 1,
              marginLeft: -5,
              borderColor: colors.secondary,
              paddingLeft: 10,
              flex: 1,
            }}
          />
          :<Span2>Você não possui histórico de pontos.</Span2>
          }
        </Container>
      </Scroll>
    </Content>
  );
};

const ListRender = (item, index) => {
  let dateformat = moment(item.Date).format('ll');

  let dateSplit = dateformat.split(",")
  console.log(dateSplit, 'dateSplit')
  return (
    <WrapView key={index}>
      <Border></Border>
      <ContentList>
        <Title>{item.Descricao}</Title>
        <Value>{item.Valor}</Value>
        <Title>{item.Pontuacao} PONTOS</Title>
      </ContentList>
      <Title>{dateSplit[0]}</Title>
    </WrapView>
  );
};

const mapStateToProps = state => ({
  pontos: state.pontos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(PontosActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PointsScene);