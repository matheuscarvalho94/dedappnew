// export default {
//   spaceregular: 'SpaceMono-Regular',
//   tradebold: 'Trade-Gothic-Bold',
//   tradeconde18: 'Trade-Gothic-Condensed-18',
//   tradecb: 'Trade-Gothic-Condensed-Bold-20',
//   tradegothic: 'Trade-Gothic',
//   tradegothicno: 'Trade-GothicNo',
// };
import {Platform} from 'react-native';

export default {
  spaceregular: Platform.OS === 'ios' ? 'Space Mono' : 'SpaceMonoRegular',
  tradebold: 'TradeGothic-Bold',
  tradeconde18: Platform.OS === 'ios' ? 'TradeGothic-Bold' : 'TradeGothicCondensed',
  tradecb: Platform.OS === 'ios' ? 'TradeGothic-Bold' : 'TradeGothicCondensedBold',
  tradegothic: 'TradeGothic',
  tradegothicno: Platform.OS === 'ios' ? 'TradeGothic-Bold' : 'TradeGothicNo',
};
