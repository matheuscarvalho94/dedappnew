import { call, put } from "redux-saga/effects";
import api from "../../config/api";

import PontosActions from '../ducks/pontos';

export function* getPontos({action}) {
  console.log(action, 'action pontos');
  try {
    const response = yield call(api.post, `/especificadores/pontuacao?codEspecificador=${action}&ano=2010`);
    // const response = yield call(api.post, `/especificadores/pontuacao?codEspecificador=767&ano=2010`);
    yield put(PontosActions.addPontosSuccess(response.data));
  } catch (err) {
    console.log(err, `errorrr`)
    yield put(PontosActions.addPontosFailure(err));
  }
}