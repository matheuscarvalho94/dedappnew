import React, { useState } from 'react';
import { TouchableOpacity, ScrollView, Alert } from 'react-native';
import { Input } from '../../component/Input';
import colors from '../../styles/colors';
import { ContentBg } from '../../styles/general';
import { Span, Container, Facebook, TextFacebook, Button, TextButton, Footer, TextFooter, Logo, H1, IndicatorLoading, WrapCheckBox, ButtonCheckBox, ContainerCheckBox } from './styles';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import TokenActions from "../../store/ducks/token";
import { facebookLogin } from '../../config/fb';

const LoginScene = ({navigation, addTokenRequest, token, addTokenfacebookRequest}) => {
  const [ login, setLogin ] = useState('');
  const [ checked, setChecked ] = useState(false);
  const [ password, setPassword ] = useState('');
  
  const _getToken = async () => {
    const body = {
      login,
      password,
      checked,
    };
    if (login === '') {
      Alert.alert('D&D', 'Digite o seu e-mail.');
    } else if (password === '') {
      Alert.alert('D&D', 'Digite sua senha.');
    } else {
      addTokenRequest(body);
    }
  };

  const handleLoginWithFacebook = async () => {
    const response = await facebookLogin();
    if (response.error) {
      return false;
    } else {
      const {user} = response;
      addTokenfacebookRequest(user);
      console.log(response, 'volta fb');
    }
  };

  return (
    <ContentBg>
      <ScrollView>
        <Container>
          <Logo
            source={require('../../assets/logo.png')}
            resizeMode="contain"
          />
          <Button onPress={() => navigation.push('RegisterScene')}>
            <TextButton>CADASTRE-SE</TextButton>
          </Button>

          <H1>JÁ POSSUI UMA CONTA? FAÇA LOGIN ABAIXO.</H1>
          <Input
            placeholder="EMAIL"
            value={login}
            placeholderTextColor={colors.white50}
            onChangeText={txt =>  setLogin(txt) }
          />
          <Input
            placeholder="SENHA"
            value={password}
            secureTextEntry
            placeholderTextColor={colors.white50}
            onChangeText={txt => setPassword(txt)}
          />

          <WrapCheckBox onPress={() => setChecked(!checked)}>
            <ButtonCheckBox>
              {checked && (
                <ContainerCheckBox />
              )}
            </ButtonCheckBox>
            <Span>Manter logado</Span>
          </WrapCheckBox>

          <Button onPress={() => _getToken()}>
            <TextButton>ENTRAR</TextButton>
            {
              token.loading && ( <IndicatorLoading size="small" color={colors.white} /> )
            }
          </Button>
          <Facebook onPress={() => handleLoginWithFacebook()}>
            <Icon name="facebook-f" color={colors.secondary} size={15} />
            <TextFacebook>ENTRAR PELO FACEBOOK</TextFacebook>
          </Facebook>
        </Container>

        <Footer>
          <TextFooter>
            RELACIONAMENTO D&D 2019 ®TODOS OS DIREITOS RESERVADOS
          </TextFooter>
        </Footer>
      </ScrollView>
    </ContentBg>
  );
};

const mapStateToProps = state => ({
  token: state.token
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(TokenActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScene);