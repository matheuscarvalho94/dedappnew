import { createStackNavigator } from 'react-navigation';
import NewsScene from '../../pages/News';

export const NewsStackNavigator = createStackNavigator({
  NewsScene: {
    screen: NewsScene,
    navigationOptions: {
      header: null
    }
  }
})