import { createStackNavigator } from 'react-navigation';
import StoreScene from '../../pages/Store';

export const StoreStackNavigator = createStackNavigator({
  StoreScene: {
    screen: StoreScene,
    navigationOptions: {
      header: null
    }
  }
})