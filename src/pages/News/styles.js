import styled from 'styled-components';
import colors from '../../styles/colors';
import { fonts } from '../../styles';

export const Padding = styled.View`
  padding: 20px 60px;
`;

export const H1 = styled.Text`
  font-family: ${fonts.tradecb};
  font-size: 60px;
  color: ${colors.secondary};
  margin: 0 0 13px;
  padding: 0 0;
`;

export const Span = styled.Text`
  font-family: ${fonts.tradeconde18};
  font-size: 16px;
  color: ${colors.white};
  margin: 0 0 35px;
  padding: 0 0;
  line-height: 21px;
`;

export const WrapNovidades = styled.View`
  border-radius: 10px;
  background: ${colors.white};
  padding: 20px 15px;
`;

export const Title = styled.Text`
  font-family: ${fonts.tradecb};
  font-size: 32px;
  color: ${colors.secondary};
  text-transform: uppercase;
  margin-top: 10px;
  margin-bottom: 35px;
  line-height: 30px;
  width: 80%;
`;

export const P = styled.Text`
  font-family: ${fonts.tradegothic};
  font-size: 14px;
  text-transform: uppercase;
  color: #999;
`;

export const Button = styled.TouchableOpacity`
  height: 45px;
  background: ${colors.secondary};
  width: 100%;
  border-radius: 10px;
  padding: 15px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 35px;
  margin-bottom: 0px;
`;

export const TextButton = styled.Text`
  font-size: 14px;
  color: ${colors.white};
  text-align: center;
  font-family: ${fonts.tradecb};
  margin-left: 0;
  text-transform: uppercase;
`;
