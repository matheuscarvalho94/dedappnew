import {call, put} from 'redux-saga/effects';
import api from '../../config/api';
import queryString from 'query-string';
import {AsyncStorage} from 'react-native';

import {Alert} from 'react-native';

import RegisterActions from '../ducks/registerFisic';

import {NavigationActions, StackActions} from 'react-navigation';
import {navigatorRef} from '../../index';

const nav = StackActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({
      routeName: 'LoginScene',
    }),
  ],
  key: null,
});

export function* register(props) {
  let dataformat = props.action.dataNascimento.split('/')
  let datacerta = dataformat[2] + '-' + dataformat[1] + '-' + dataformat[0];
  let cpfreplace = props.action.cpf.replace('.', '').replace('.', '').replace('-', '')

  const body = queryString.stringify({
    nome: props.action.nome,
    sobrenome: props.action.sobrenome,
    sexo: props.action.sexo2 === true ? 'Masculino' : 'Feminino',
    estadoCivil: props.action.estadoCivil,
    nascimento: datacerta,
    email: props.action.email,
    login: props.action.email,
    cpfcnpj: cpfreplace,
    senha: props.action.password,
    tipo: props.action.tipo,
    dataCadastro: props.action.dataCadastro,
    codigo: props.action.codigo,
    aprovado: props.action.aprovado,
    formacao: props.action.formacao,
    telefoneCelular: props.action.celular,
    endereco: props.action.endereco,
    cep: props.action.cep,
    bairro: props.action.bairro,
    numero: props.action.numero,
    cidade: props.action.cidade,
    uf: props.action.uf,
    complemento : props.action.complemento
  });

  try {
    console.log('aci', body);
    const response = yield call(
      api.post,
      `/cadastro/cadastropessoafisica`,
      body,
    );
    yield put(RegisterActions.addRegisterSuccess(response.data));
    Alert.alert('Sucesso', 'Cadastro realizado com sucesso!');
    //redirecionar aqui
    navigatorRef.dispatch(nav);

  } catch (err) {
    Alert.alert('Erro', err.response.data.Message);
    yield put(RegisterActions.addRegisterFailure(err));
  }
}
