import React from 'react';
import {H1, Span, WrapNovidades, Title, P, Button, TextButton,Padding} from './styles';
import {Content, Container, Scroll} from '../../styles/general';
import Header from '../../component/Header';

const NewsScene = ({navigation}) => {
  return (
    <Content>
      <Header navigation={navigation} />
      <Scroll>
        <Padding>
          <H1>NOVIDADES</H1>
          <Span>LOREM IMPSUN DOLOR SIT AMET, CONSECTEUR ADISCIPING ELIT, SED DO EIU LOREM IMPSUN DOLOT IT SIT ATMEET, CONSECTEUR ADISCIPING ELIT, SED DO EIU</Span>

          <WrapNovidades>
            <Title>Bota fora D&D começa em junho</Title>
            <P>design arte e estilo com até 50% off em todo shopping</P>
            <Button>
              <TextButton>ver mais+</TextButton>
            </Button>
          </WrapNovidades>
          
        </Padding>
      </Scroll>
    </Content>
  )
}

export default NewsScene;
