import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* Types & Action Creators */
const { Types, Creators } = createActions({
  addMydataRequest: ["action"],
  addMydataSuccess: ["mydata"],
  addMydataFailure: ["mydata"],
  addGetdataRequest: ["mydata"],
  addGetdataSuccess: ["mydata"],
});


export const MydataTypes = Types;
export default Creators;

/* Initial State */

export const INITIAL_STATE = Immutable({
  data: [],
  loading: false,
  dataadapter : []
});

/* Reducers */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_MYDATA_REQUEST]: state => state.merge({ loading: true }),
  [Types.ADD_MYDATA_SUCCESS]: (state: Object, { mydata }: any) => state.merge({ loading: false,  data: mydata }),
  [Types.ADD_MYDATA_FAILURE]: (state: Object, { mydata }: any) => state.merge({ loading: false,  data: mydata }),
  [Types.ADD_GETDATA_REQUEST]: (state: Object, { mydata }: any) => state.merge({ loading: false,  data: mydata }),
  [Types.ADD_GETDATA_SUCCESS]: (state: Object, { mydata }: any) => state.merge({ loading: false,  dataadapter: mydata })
});