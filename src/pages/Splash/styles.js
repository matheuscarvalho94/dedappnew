import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';
import styled from 'styled-components';

export const Logo = styled.Image`
  text-align: center;
  margin-bottom: 35px;
  width: 100%;
  height: 120px;
`;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.black,
  },
  progressContainer: {
    width: '80%',
    justifyContent: 'center',
    flexDirection: 'column',
  },
});
