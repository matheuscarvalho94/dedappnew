import { createStackNavigator } from 'react-navigation';
import ContactUs from '../../pages/ContactUs';

export const BenefitsStackNavigator = createStackNavigator({
  ContactUs: {
    screen: ContactUs,
    navigationOptions: {
      header: null
    }
  }
})