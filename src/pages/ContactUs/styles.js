import styled from 'styled-components';
import colors from '../../styles/colors';
import { fonts } from '../../styles';

export const Padding = styled.View`
  padding: 20px 40px;
`;

export const H1 = styled.Text`
  font-family: ${fonts.tradecb};
  font-size: 42px;
  color: ${colors.secondary};
  margin: 0 0 0;
  padding: 0 0;
`;

export const Span = styled.Text`
  font-family: ${fonts.tradeconde18};
  font-size: 16px;
  color: ${colors.white};
  margin: 0 0 35px;
  padding: 0 0;
  line-height: 21px;
`;

export const Button = styled.TouchableOpacity`
  height: 65px;
  background: ${colors.secondary};
  width: 100%;
  border-radius: 10px;
  padding: 15px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 0;
  margin-bottom: 0px;
`;

export const TextButton = styled.Text`
  font-size: 18px;
  color: ${colors.white};
  text-align: center;
  font-family: ${fonts.tradecb};
  margin-left: 0;
  text-transform: uppercase;
`;
