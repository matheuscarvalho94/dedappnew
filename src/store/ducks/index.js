import { combineReducers } from "redux";
import { reducer as token } from "./token";
import { reducer as contato } from "./contato";
import { reducer as pontos } from "./pontos";
import { reducer as mydata } from "./mydata";
import { reducer as register } from "./registerFisic";
import { reducer as registerJuridic } from "./registerJuridic";

const rootReducers = combineReducers({
  contato,
  pontos,
  token,
  mydata,
  register,
  registerJuridic,
});

export default rootReducers;
