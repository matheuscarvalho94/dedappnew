import styled from 'styled-components';
import colors from '../../styles/colors';
import { fonts } from '../../styles';

import { StyleSheet } from 'react-native';


export const Padding = styled.View`
  padding: 20px 40px;
`;

export const H1 = styled.Text`
  font-family: ${fonts.tradecb};
  font-size: 42px;
  color: ${colors.secondary};
  margin: 0 0 0;
  padding: 0 0;
`;

export const Span = styled.Text`
  font-family: ${fonts.tradeconde18};
  font-size: 16px;
  color: ${colors.white};
  margin: 0 0 35px;
  padding: 0 0;
  line-height: 21px;
`;

export const Button = styled.TouchableOpacity`
  height: 65px;
  background: ${colors.secondary};
  width: 100%;
  border-radius: 10px;
  padding: 15px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 0;
  margin-bottom: 0px;
`;

export const TextButton = styled.Text`
  font-size: 18px;
  color: ${colors.white};
  text-align: center;
  font-family: ${fonts.tradecb};
  margin-left: 0;
  text-transform: uppercase;
`;

export const Logo = styled.Image`
  text-align: center;
  margin-bottom: 35px;
  width: 100%;
  height: 80px;
`;

export const Container = styled.View`
  padding: 40px 0 0;
  margin: 0 auto;
  width: 90%;
`;


export const ContainerCenter = styled.View`
  display: flex;
  flex: 1;
  width: 80%;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding-top: 20px;
`;

export const Header = styled.View`
  padding: 20px;
  flex-direction: row;
  align-items: center;
  width: 100%;
`;

export const H3 = styled.Text`
  font-size: 18px;
  color: ${colors.white};
  font-family: ${fonts.tradebold};
  margin: 0 auto 15px;
  text-align: center;
`;

export const Form = styled.View`
  padding: 0;
  background: ${colors.white};
  border-radius: 10px;
  width: 90%;
  margin: 20px auto;
`;

export const Label = styled.Text`
  font-family: ${fonts.tradeconde18};
  color: ${colors.white50};
  font-size: 16px;
  padding-left: 15px;
  margin-bottom: 10px;
`;

export const ButtonAdd = styled.Text`
  font-family: ${fonts.tradebold};
  color: ${colors.white};
  font-size: 18px;
  margin: 30px auto 0px;
`;

export const styles = StyleSheet.create({
  input: {
    height: 60,
    borderWidth: 1,
    borderColor: colors.white,
    width: '100%',
    backgroundColor: 'transparent',
    fontSize: 16,
    fontFamily: fonts.tradeconde18,
    color: colors.white50,
    borderRadius: 10,
    paddingTop: 18,
    paddingHorizontal: 25,
    paddingBottom: 15,
    marginBottom: 10,
  }
})