import React, {useEffect} from 'react';
import {ActivityIndicator} from 'react-native';
import './config/ReactotronConfig';

import {Provider} from 'react-redux';
import store from './store';
// import {PersistGate} from 'redux-persist/es/integration/react';
// import store, {configureStore} from './store';
// const {persistor} = configureStore();

import Routes from './routes/routes';

export let navigatorRef;

const App = () => {
  useEffect(() => {
    navigatorRef = navigatorRef;
  }, []);

  return (
    <Provider store={store}>
      {/* <PersistGate loading={<ActivityIndicator />} persistor={persistor} > */}
        <Routes
          ref={nav => {
            navigatorRef = nav;
          }}
        />
      {/* </PersistGate> */}
    </Provider>
  );
};

export default App;
