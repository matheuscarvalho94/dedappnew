import React, {useState, useEffect} from 'react';
import {ScrollView, Alert} from 'react-native';
import {Content, Container} from '../../styles/general';
import Header from '../../component/Header';
import {H1, Span, Padding, Button, TextButton} from './styles';
import {Input} from '../../component/Input';
import {colors} from '../../styles';


import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import ContatoActions from '../../store/ducks/contato';
import Store from '../../store';

function ContactUsScene({navigation, addContatoRequest, contato}) {

  useEffect(() => {
    console.log(Store.getState().mydata.dataadapter.userViewModel, 'Store.getState().token.data.userViewModel.Email')
  })

  const [message, setMessage] = useState('');
  const [origem, setOrigem] = useState('app_relacionamento');

  function _sendMessage(){

    let email = Store.getState().mydata.dataadapter.userViewModel.Email;
    let nome = Store.getState().mydata.dataadapter.userViewModel.Nome;
    let assunto = "APP - Relacionamento";

    const body = {
      email,
      nome,
      assunto,
      mensagem: message,
      origem,
    };

    //console.log(body)

    if (message === '') {
      Alert.alert('D&D', 'Digite mensagem')
    } else {
      addContatoRequest(body);
    }
  };

  return (
    <Content>
      <Header navigation={navigation} />
      <ScrollView style={{backgroundColor: '#000', flex: 1}}>
        <Padding>
          <H1>FALE{`\n`}CONOSCO</H1>
          <Span>
            LOREM IMPSUN DOLOR SIT AMET, CONSECTEUR ADISCIPING ELIT, SED DO EIU
            LOREM IMPSUN DOLOT IT SIT ATMEET, CONSECTEUR ADISCIPING ELIT, SED DO
            EIU
          </Span>

          <Input
            placeholder="MENSAGEM"
            value={message}
            placeholderTextColor={colors.white50}
            multiline={true}
            onChangeText={txt => setMessage(txt)}
            style={{
              paddingBottom: 160,
              paddingTop: 20,
              alignItems: 'flex-start',
              height: 'auto',
              marginBottom: 10,
              lineHeight: 25,
            }}
          />

          <Button onPress={() => _sendMessage()}>
            <TextButton>ENVIAR</TextButton>
          </Button>
        </Padding>
      </ScrollView>
    </Content>
  );
}

const mapStateToProps = state => ({
  contato: state.contato
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(ContatoActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactUsScene);
