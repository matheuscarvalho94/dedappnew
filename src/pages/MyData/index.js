import React, {useState, useEffect} from 'react';
import {ScrollView, View, StyleSheet, Alert} from 'react-native';
import {Content, Container} from '../../styles/general';
import Header from '../../component/Header';
import {H1, Span, Padding, Button, TextButton, Label, styles} from './styles';

import {Input} from '../../component/Input';
import {colors, fonts} from '../../styles';

import Radio from '../../component/Radio';
import PickerSelect from '../../component/Picker';
import RNPickerSelect from 'react-native-picker-select';

import Store from '../../store';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import MyDataActions from '../../store/ducks/mydata';

import {TextInputMask} from 'react-native-masked-text';

function MyDataScene({navigation, addMydataRequest, addGetdataRequest, data}) {
  console.log('haaaaa', data);

  useEffect(() => {
    addGetdataRequest(Store.getState().mydata.dataadapter.userViewModel.Cod);
  }, []);

  useEffect(() => {
    if (data.dataadapter.userViewModel) {
      console.log(data.dataadapter.userViewModel, 'data.dataadapter.userViewModel')
      let dtNasc = data.dataadapter.userViewModel.Nascimento ? data.dataadapter.userViewModel.Nascimento.substring(
        0,
        10,
      ).split('-') : null;

      setNome(data.dataadapter.userViewModel.Nome);
      setSobrenome(data.dataadapter.userViewModel.Sobrenome);
      setEmail(data.dataadapter.userViewModel.Email);
      if (data.dataadapter.userViewModel.Sexo === 'Masculino') {
        setSexo1(false);
        setSexo2(true);
      } else if (data.dataadapter.userViewModel.Sexo === 'Feminino'){
        setSexo1(true);
        setSexo2(false);
      }
      setDataNascimento(dtNasc === null ? null : dtNasc[2] + '/' + dtNasc[1] + '/' + dtNasc[0]);

      setEstadoCivil(data.dataadapter.userViewModel.EstadoCivil);
      setCpf(data.dataadapter.userViewModel.Cpfcnpj);
      //setPassword(data.dataadapter.userViewModel.Senha);
      setTipo(data.dataadapter.userViewModel.Tipo);
      setCelular(data.dataadapter.userViewModel.TelefoneCelular);
      setCep(data.dataadapter.userViewModel.Cep);
      setCidade(data.dataadapter.userViewModel.Cidade);
      setEstado(data.dataadapter.userViewModel.Uf);
      setBairro(data.dataadapter.userViewModel.Bairro);
      setNumero(data.dataadapter.userViewModel.Numero);
      setComplemento(data.dataadapter.userViewModel.Complemento);

      //combos
      setFormacao(data.dataadapter.userViewModel.Formacao);
      setAssociacao(data.dataadapter.userViewModel.Associacao);
      setAssociacao(data.dataadapter.userViewModel.Associacao);
    }
  }, [data.dataadapter.userViewModel]);

  const stateCivil = [
    {
      value: 'Solteiro(a)',
      label: 'SOLTEIRO(A)',
    },
    {
      value: 'Casado(a)',
      label: 'CASADO(A)',
    },
    {
      value: 'Divorciado(a)',
      label: 'DIVORCIADO(A)',
    },
    {
      value: 'Separado(a)',
      label: 'SEPARADO(A)',
    },
    {
      value: 'Viúvo(a)',
      label: 'VIÚVO(A)',
    },
  ];

  const stateFormacao = [
    {
      value: 'Arquitetura',
      label: 'ARQUITETURA',
    },
    {
      value: 'Designer de Interiores',
      label: 'DESIGNER DE INTERIORES',
    },
    {
      value: 'Paisagista',
      label: 'PAISAGISTA',
    },
    {
      value: 'Decorador',
      label: 'DECORADOR',
    },
    {
      value: 'Outros',
      label: 'OUTROS',
    },
  ];

  const stateAssociacao = [
    {
      value: 'CREA',
      label: 'CREA',
    },
    {
      value: 'ABD',
      label: 'ABD',
    },
    {
      value: 'ASBEA',
      label: 'ASBEA',
    },
    {
      value: 'CAU',
      label: 'CAU',
    },
    {
      value: 'Outros',
      label: 'Outros',
    },
  ];

  // const [dados, setDados] = useState({
  //   name: Store.getState().token.data.userViewModel.Nome,
  //   email: Store.getState().token.data.userViewModel.Email,
  //   cep: Store.getState().token.data.userViewModel.Cep,
  //   uf: Store.getState().token.data.userViewModel.UF,
  //   cidade: Store.getState().token.data.userViewModel.Cidade,
  // });

  const [nome, setNome] = useState();
  //Store.getState().token.data.userViewModel.Nome,
  //this.props.mydata.userViewModel.nome
  //mydata.data.userViewModel.Nome`
  const [sobrenome, setSobrenome] = useState(
    //Store.getState().token.data.userViewModel.Sobrenome,
    '',
  );
  const [email, setEmail] = useState(
    //Store.getState().token.data.userViewModel.Email,
    '',
  );
  const [sexo1, setSexo1] = useState(false);
  const [sexo2, setSexo2] = useState(false);
  const [dataNascimento, setDataNascimento] = useState(
    //Store.getState().token.data.userViewModel.Nascimento,
    '',
  );
  const [estadoCivil, setEstadoCivil] = useState(
    //Store.getState().token.data.userViewModel.EstadoCivil,
    '',
  );
  const [cpf, setCpf] = useState(
    //Store.getState().token.data.userViewModel.Cpfcnpj,
    '',
  );

  const [password, setPassword] = useState('');
  const [tipo, setTipo] = useState(
    //Store.getState().token.data.userViewModel.Tipo,
    '',
  );

  const [dataCadastro, setDataCadastro] = useState(Store.getState().mydata.dataadapter.userViewModel.DataCadastro);
  const [codigo, setCodigo] = useState(Store.getState().mydata.dataadapter.userViewModel.Codigo);
  const [aprovado, setAprovado] = useState('1');

  const [celular, setCelular] = useState(
    //Store.getState().token.data.userViewModel.TelefoneCelular,
    '',
  );
  const [formacao, setFormacao] = useState(
    //Store.getState().token.data.userViewModel.Formacao,
    '',
  );
  const [associacao, setAssociacao] = useState(
    //Store.getState().token.data.userViewModel.Associacao,
    '',
  );

  const [cep, setCep] = useState(
    //Store.getState().token.data.userViewModel.Cep
    '',
  );

  const [estado, setEstado] = useState(
    //Store.getState().token.data.userViewModel.Uf,
    '',
  );
  const [cidade, setCidade] = useState(
    //Store.getState().token.data.userViewModel.Cidade,
    '',
  );
  const [bairro, setBairro] = useState(
    //Store.getState().token.data.userViewModel.Bairro,
    '',
  );
  const [endereco, setEndereco] = useState(
    //Store.getState().token.data.userViewModel.Endereco,
    '',
  );
  const [numero, setNumero] = useState(
    //Store.getState().token.data.userViewModel.Numero,
    '',
  );
  const [complemento, setComplemento] = useState(
    //Store.getState().token.data.userViewModel.Complemento,
    '',
  );

  function onSetSexo1() {
    setSexo1(true);
    setSexo2(false);
  }

  function onSetSexo2() {
    setSexo1(false);
    setSexo2(true);
  }

  function valueChange(value) {
    setEstadoCivil(value);
  }

  function valueChangeForm(value) {
    setFormacao(value);
  }

  function valueChangeAsso(value) {
    setAssociacao(value);
  }

  const _update = () => {
    const body = {
      nome,
      sobrenome,
      email,
      sexo1,
      sexo2,
      estadoCivil,
      dataNascimento,
      cpf,
      password,
      tipo,
      dataCadastro,
      cod: Store.getState().token.data.userViewModel.Cod,
      codigo: Store.getState().token.data.userViewModel.Codigo,
      aprovado,
      formacao,
      associacao,
      endereco,
      cep,
      bairro,
      numero,
      cidade,
      // uf,
    };

    //console.log(body)

    if (nome === '') {
      Alert.alert('D&D', 'Digite o seu nome.');
    } else if (email === '') {
      Alert.alert('D&D', 'Digite seu email.');
    } else if (password === '') {
      Alert.alert('D&D', 'Digite sua senha.');
    } else {
      //console.log(body)
      addMydataRequest(body);
    }
  };

  return (
    <Content>
      <Header navigation={navigation} />
      <ScrollView style={{backgroundColor: '#000', flex: 1}}>
        <Padding>
          <H1>MEUS{`\n`}DADOS</H1>
          <Span>
            LOREM IMPSUN DOLOR SIT AMET, CONSECTEUR ADISCIPING ELIT, SED DO EIU
            LOREM IMPSUN DOLOT IT SIT ATMEET, CONSECTEUR ADISCIPING ELIT, SED DO
            EIU
          </Span>

          {/* <Input
            placeholder="NOME"
            value={name}
            closeButton
            clear={() => setName('')}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setName(txt)}
          />

          <Input
            placeholder="EMAIL"
            value={email}
            closeButton
            clear={() => setEmail('')}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setEmail(txt)}
          />

          <Input
            placeholder="CEP"
            value={cep}
            closeButton
            clear={() => setCep('')}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setCep(txt)}
          />

          <Input
            placeholder="UF"
            value={uf}
            closeButton
            clear={() => setUf('')}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setUf(txt)}
          />

          <Input
            placeholder="CIDADE"
            value={cidade}
            closeButton
            clear={() => setCidade('')}
            placeholderTextColor={colors.white50}
            onChangeText={txt => setDados.cidade(txt)}
          /> */}

          <Container>
            <Input
              placeholder="NOME"
              value={nome}
              placeholderTextColor={colors.white50}
              onChangeText={txt => setNome(txt)}
            />
            <Input
              placeholder="SOBRENOME"
              value={sobrenome}
              placeholderTextColor={colors.white50}
              onChangeText={txt => setSobrenome(txt)}
            />

            <View style={{marginTop: 15, marginBottom: 30}}>
              <Label>Sexo</Label>
              <View style={{flexDirection: 'row'}}>
                <Radio
                  label="FEMININO"
                  value={sexo1}
                  onPress={() => onSetSexo1()}
                />
                <Radio
                  label="MASCULINO"
                  value={sexo2}
                  onPress={() => onSetSexo2()}
                />
              </View>
            </View>

            {/* <PickerSelect
              label="ESTADO CIVIL"
              value={estadoCivil}
              list={stateCivil}
              valueChange={valueChange}
              placeholderTextColor={colors.white50}
            /> */}

            <View style={[stylesRNP.containerSelect]}>
              <RNPickerSelect
                placeholder={{
                  label: 'ESTADO CIVIL',
                  value: '',
                }}
                style={pickerStyle}
                onValueChange={value => valueChange(value)}
                items={stateCivil}
                value={estadoCivil}
              />
            </View>

            <TextInputMask
              type={'datetime'}
              options={{
                format: 'DD/MM/YYYY',
              }}
              value={dataNascimento}
              style={styles.input}
              onChangeText={text => setDataNascimento(text)}
              placeholderTextColor={colors.white50}
              placeholder="DATA DE NASCIMENTO"
            />

            <Input
              placeholder="EMAIL"
              value={email}
              placeholderTextColor={colors.white50}
              onChangeText={txt => setEmail(txt)}
            />

            <TextInputMask
              type={'cpf'}
              value={cpf}
              style={styles.input}
              onChangeText={text => setCpf(text)}
              placeholderTextColor={colors.white50}
              placeholder="CPF"
            />

            <TextInputMask
              type={'cel-phone'}
              options={{
                maskType: 'BRL',
                withDDD: true,
                dddMask: '(99) ',
              }}
              value={celular}
              style={styles.input}
              onChangeText={text => setCelular(text)}
              placeholderTextColor={colors.white50}
              placeholder="CELULAR"
            />

            {/* <PickerSelect
              label="FORMAÇÃO"
              value={formacao}
              list={stateFormacao}
              valueChange={valueChangeForm}
              placeholderTextColor={colors.white50}
            />

            <PickerSelect
              label="ASSOCIAÇÃO"
              value={associacao}
              list={stateAssociacao}
              valueChange={valueChangeAsso}
              placeholderTextColor={colors.white50}
            /> */}

            <View style={[stylesRNP.containerSelect]}>
              <RNPickerSelect
                placeholder={{
                  label: 'FORMAÇÃO',
                  value: '',
                }}
                style={pickerStyle}
                onValueChange={value => valueChangeForm(value)}
                items={stateFormacao}
                value={formacao}
              />
            </View>

            <View style={[stylesRNP.containerSelect]}>
              <RNPickerSelect
                placeholder={{
                  label: 'ASSOCIAÇÃO',
                  value: '',
                }}
                style={pickerStyle}
                onValueChange={value => valueChangeAsso(value)}
                items={stateAssociacao}
                value={associacao}
              />
            </View>

            <TextInputMask
              type={'zip-code'}
              value={cep}
              style={styles.input}
              onChangeText={text => setCep(text)}
              placeholderTextColor={colors.white50}
              placeholder="CEP"
            />

            <Input
              placeholder="ESTADO"
              value={estado}
              placeholderTextColor={colors.white50}
              onChangeText={txt => setEstado(txt)}
            />

            <Input
              placeholder="CIDADE"
              value={cidade}
              placeholderTextColor={colors.white50}
              onChangeText={txt => setCidade(txt)}
            />

            <Input
              placeholder="BAIRRO"
              value={bairro}
              placeholderTextColor={colors.white50}
              onChangeText={txt => setBairro(txt)}
            />

            <Input
              placeholder="NÚMERO"
              value={numero}
              placeholderTextColor={colors.white50}
              onChangeText={txt => setNumero(txt)}
            />

            <Input
              placeholder="COMPLEMENTO"
              value={complemento}
              placeholderTextColor={colors.white50}
              onChangeText={txt => setComplemento(txt)}
            />

            <Input
              placeholder="SENHA"
              value={password}
              secureTextEntry={true}
              placeholderTextColor={colors.white50}
              onChangeText={txt => setPassword(txt)}
            />

            <Button onPress={() => _update()}>
              <TextButton> ENVIAR </TextButton>
            </Button>
          </Container>
        </Padding>
      </ScrollView>
    </Content>
  );
}

const mapStateToProps = state => ({
  data: state.mydata,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(MyDataActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyDataScene);

const stylesRNP = StyleSheet.create({
  containerSelect: {
    backgroundColor: 'transparent',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.white,
    height: 60,
    marginBottom: 15,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 3,
  },
  select: {
    height: 35,
    //fontFamily: fonts.tradeconde18,
    fontSize: 12,
    color: colors.white50,
    width: '100%',
  },
});

const pickerStyle = {
  font: fonts.tradeconde18,
  placeholder: {
    color: colors.white50,
    fontSize: 16,
    fontFamily: fonts.tradeconde18,
  },
  inputIOS: {
    color: colors.white50,
    fontFamily: fonts.tradeconde18,
    fontSize: 14,
    padding: 0,
  },
  inputAndroid: {
    color: colors.white50,
    fontFamily: fonts.tradeconde18,
    fontSize: 14,
    padding: 0,
  },
};
